# JSONAPI PoC

## Getting started

- [Tilt.dev](https://tilt.dev/)

### Local setup

```bash
git clone https://gitlab.com/govcms-poc/jsonapi-poc.git govcms-jsonapi-poc
cd govcms-jsonapi-poc/
tilt up
```

### Local hosts

You can edit your local host or use the localhost with different ports to access the local services

```bash
# GovCMS API
127.0.0.1 govcmsd9.govcms.local
```